const express = require('express');
const bcrypt = require('bcrypt');
const folderWatcher = require('../folderWatcher')();
const watcherLog = require('../models/watcherLog');
const userModel = require('../models/user');

const hashPassword = (password) => {
    return bcrypt.hashSync(password, bcrypt.genSaltSync());
}

const compareHashPassword = (inputPassword, hashedPassword) => {
    return bcrypt.compareSync(inputPassword, hashedPassword);
}

const router = express.Router();

router.get('/status/:event', (req, res, next) => {
    watcherLog.find({ event: req.params.event }).then((logMsgs) => {
        res.send(logMsgs);
    });
});

router.post('/start/:folder', (req, res, next) => {
    folderWatcher.startWatching(req.params.folder);
    res.send('Started spying');
});

router.post('/stop', (req, res, next) => {
    folderWatcher.stopWatching();
    res.send('Stopped spying');
});

router.post('/user/create', (req, res, next) => {
    if (req.body.email
        && req.body.username
        && req.body.password) {
        const userData = {
            email: req.body.email,
            username: req.body.username,
            password: hashPassword(req.body.password),
        };

        userModel.create(userData, (err, user) => {
            if (err) {
                return next(err);
            }

            return res.send(`user created \r\n ${user}`);
        });
    }
});

router.post('/login', (req, res) => {
    const { username, password } = req.body;

    userModel.findOne({ username }).then((user) => {
        if (!user) {
            res.redirect('/login');
        } else if (!compareHashPassword(password, user.password)) {
            res.redirect('/login');
        } else {
            req.session.user = user;
            res.redirect('/index.html');
        }
    });
});

router.post('/logout', (req, res) => {
    res.clearCookie('user_sid');
    res.redirect('/login.html');
});

module.exports = router;