const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const UserSchema = new Schema({
    email: {
        type: String,
        unique: true,
        required: false,
        trim: true,
    },
    username: {
        type: String,
        unique: true,
        required: true,
        trim: true,
    },
    password: {
        type: String,
        required: true,
     },
     //,
    // passwordConf: {
    //     type: String,
    //     required: false,
    // }
});

const User = mongoose.model('User', UserSchema);

module.exports = User;
