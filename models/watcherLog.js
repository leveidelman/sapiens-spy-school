const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const WatcherLogSchema = new Schema({
    date: Date,
    level: String,
    message: String,
    event: String,
});

const WatcherLog = mongoose.model('watcherLog', WatcherLogSchema);

module.exports = WatcherLog;
