module.exports = {};

const WatcherLog = require('./models/watcherLog');

module.exports.info = (msg, event) => {
    WatcherLog.create({
        date: Date.now(),
        level: 'INFO',
        message: msg,
        event,
    });
};