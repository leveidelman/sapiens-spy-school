const express = require('express');
const bodyParser = require('body-parser');
const mongoose = require('mongoose');
const session = require('express-session');
const cookieParser = require('cookie-parser');

const app = express();

mongoose.connect('mongodb://localhost/spySchool')
mongoose.Promise = global.Promise;

app.use(express.static('public'));
app.use(bodyParser.json());
app.use(cookieParser());
app.use(session({
  key: 'user_sid',
  secret: 'secret',
  resave: false,
  saveUninitialized: false,
  cookie: {
    expires: 600000,
  },
}));
app.use('/api', require('./routes/api'));

app.use((req, res, next) => {
  if (req.cookies.user_sid && !req.session.user) {
    res.clearCookie('user_sid');
  }
  next();
});
app.use((err, req, res, next) => {
  res.status(422).send({
    error: err.message,
  });
});

app.listen(3000, (err) => {
  if (err) {
    throw err;
  }

  console.log('Server is up on port 3000');
});