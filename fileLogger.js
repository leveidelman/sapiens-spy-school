let logger = require('simple-node-logger');

module.exports = {};

module.exports.fileLogger = (fileName, level) => {
    logger = logger.createSimpleFileLogger(fileName);
    if (level) {
        logger.setLevel(level);
    }

    return logger;
};

module.exports.info = (msg) => {
    module.exports.fileLogger.info(msg);
};