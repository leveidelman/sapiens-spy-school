const chokidar = require('chokidar');
const fileLogger = require('./fileLogger').fileLogger('./log/watcher.log');
const dbLogger = require('./dbLogger');

module.exports = () => {
    let watcher = {};

    const startWatching = (folderToWatch) => {
        watcher = chokidar.watch(folderToWatch, {
            ignored: /(^|[\/\\])\../,
            persistent: true,
        }).on('add', (path) => {
            fileLogger.info(`File ${path} has been added`);
            dbLogger.info(`File ${path} has been added`, 'ADD');
        })
            .on('change', (path) => {
                fileLogger.info(`File ${path} has been changed`);
                dbLogger.info(`File ${path} has been changed`, 'CHANGE');
            })
            .on('unlink', (path) => {
                fileLogger.info(`File ${path} has been removed`);
                dbLogger.info(`File ${path} has been removed`, 'REMOVE');
            });

        fileLogger.info(`Started watching: ${folderToWatch}`);
        dbLogger.info(`Started watching: ${folderToWatch}`);
        console.log('Started spying');
    };

    const stopWatching = () => {
        watcher.close();
        fileLogger.info('Stopped watching');
        dbLogger.info('Stopped watching');
        console.log('Stopped spying');
    };

    return {
        startWatching,
        stopWatching,
    };
};